import schema.Item;
import schema.Order;

import java.util.ArrayList;
import java.util.List;

public class AssistantManager implements HandleOrders {
    public static final String NAME = "am";
    private HandleOrders handler;

    public AssistantManager(HandleOrders handler) {
        this.handler = handler;
    }

    public void fillPrice(Order o) {
        List<Item> itemsWithPrices = new ArrayList<Item>();
        for (Item i : o.getItems()) {
            itemsWithPrices.add(new Item(i, getPriceFor(i)));
        }
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Filling price for order " + o.getTableNumber() + " of type " + o.getType());
        Order order = new Order(o, itemsWithPrices, o.getDuration());
        order.setAmDone(true);
        handler.handle(order);
    }

    private String getPriceFor(Item i) {
        return "1.0";
    }

    @Override
    public void handle(Order order) {
        fillPrice(order);
    }
}
