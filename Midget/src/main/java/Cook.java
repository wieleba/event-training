import schema.*;

import java.util.*;

public class Cook implements HandleOrders {
    private HandleOrders handler;
    private int sleepTime;
    public static String NAME = "cook";

    public Cook(HandleOrders handler, int sleepTime) {
        this.handler = handler;
        this.sleepTime = sleepTime;
    }

    private void prepareMealFor(Order o) {
        List<Item> itemsWithIngredients = new ArrayList<Item>();
        for (Item i : o.getItems()) {
            List<Ingredient> ingredients = getIgredientsFor(i);
            prepareMeal(i.getName(), ingredients);
            itemsWithIngredients.add(new Item(i, ingredients));
        }
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Order order = new Order(o, itemsWithIngredients, 50);
        order.setCookDone(true);
        handler.handle(order);
    }

    private void prepareMeal(String name, List<Ingredient> ingredients) {
        System.out.println("Preparing meal");
    }

    private List<Ingredient> getIgredientsFor(Item i) {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void handle(Order order) {
        prepareMealFor(order);
    }
}
