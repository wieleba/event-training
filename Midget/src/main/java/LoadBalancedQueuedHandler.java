import schema.Order;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 07.02.13
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
public class LoadBalancedQueuedHandler implements HandleOrders {
    private Statistic[] handlers;

    public LoadBalancedQueuedHandler(Statistic... handlers) {
        this.handlers = handlers;
    }

    @Override
    public void handle(Order order) {
        int minIndex = 0;
        int minValue = Integer.MAX_VALUE;
        for (int i = 0; i < handlers.length; i++) {
            Statistic handler = handlers[i];
            if (minValue > handler.queueSize()) {
                minValue = handler.queueSize();
                minIndex = i;
            }
        }
        handlers[minIndex].handle(order);
    }
}
