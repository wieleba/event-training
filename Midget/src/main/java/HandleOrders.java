import schema.Order;


public interface HandleOrders {

    void handle(Order order);

}
