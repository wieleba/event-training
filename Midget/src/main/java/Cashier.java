import schema.Item;
import schema.Order;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Cashier implements HandleOrders {
    public static final String NAME = "cashier";
    private HandleOrders handler;
    Map<String, Order> pendingOrders = new ConcurrentHashMap<String, Order>();

    public Cashier(HandleOrders handler) {
        this.handler = handler;
    }

    @Override
    public void handle(Order order) {
        pendingOrders.put(order.getId(), order);
    }

    public void payFor(String orderId) {
        Order order = pendingOrders.get(orderId);
        if (order == null) {
            throw new IllegalStateException("Order not found [" + orderId + "]");
        }
        order.setCashierDone(true);
        System.out.println("Order of type " + order.getType() + " paid");
        handler.handle(order.paid("CASH"));
    }

    private String getPriceFor(Item i) {
        return "1.0";
    }

}
