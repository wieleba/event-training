import schema.Order;

public class RoundRobinHandler implements HandleOrders {

    private HandleOrders[] handlers;
    private int current = 0;

    public RoundRobinHandler(HandleOrders... handlers) {
        this.handlers = handlers;
    }

    @Override
    public void handle(Order order) {
        handlers[current % handlers.length].handle(order);
        current++;
    }
}
