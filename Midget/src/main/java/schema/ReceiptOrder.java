package schema;

public class ReceiptOrder {

    public static final ReceiptOrder EMPTY = new ReceiptOrder("", "", "");

    public ReceiptOrder(String method, String tip, String amount) {
        this.method = method;
        this.tip = tip;
        this.amount = amount;
    }

    public ReceiptOrder(ReceiptOrder r) {
        this.method = r.method;
        this.tip = r.tip;
        this.amount = r.amount;
    }

    private String method;
    private String tip;
    private String amount;

    public ReceiptOrder(String method) {
        this.method = method;
    }
}
