package schema;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 07.02.13
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement
public class Item {

    public Item(String name, int qty, String price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public Item(Item i) {
        this.name = i.name;
        this.qty = i.qty;
        this.price = i.price;
    }

    private String name;
    private int qty;
    private String price;
    private List<Ingredient> ingredients;

    public Item(Item i, List<Ingredient> ingredients) {
        this(i);
        this.ingredients = ingredients;
    }

    public Item(Item i, String price) {
        this(i);
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getQty() {
        return qty;
    }

    public String getPrice() {
        return price;
    }

    public List<Ingredient> getIngredients() {
        return Collections.unmodifiableList(ingredients);
    }
}
