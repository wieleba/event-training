package schema;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "order")

public class Order {

    private int duration;

    @XmlElement
    private String tableNumber;

    @XmlElement
    private List<Item> items = new ArrayList<Item>();

    @XmlElement
    private String total;

    @XmlElement
    private ReceiptOrder receipt;

    @XmlElement
    private String tax;

    @XmlElement
    private boolean cookDone;
    private boolean amDone;
    private boolean cashierDone;
    private String type;

    public Order(String tableNumber, List<Item> items, String type) {
        this(tableNumber, items);
        this.type = type;
    }

    public boolean isCookDone() {
        return cookDone;
    }

    public void setCookDone(boolean cookDone) {
        this.cookDone = cookDone;
    }

    public boolean isAmDone() {
        return amDone;
    }

    public void setAmDone(boolean amDone) {
        this.amDone = amDone;
    }

    public boolean isCashierDone() {
        return cashierDone;
    }

    public void setCashierDone(boolean cashierDone) {
        this.cashierDone = cashierDone;
    }

    public Order(String tableNumber, List<Item> items, String total, String tax, ReceiptOrder receipt) {
        this.tableNumber = tableNumber;
        this.items = items;
        this.total = total;
        this.tax = tax;
        this.receipt = receipt;
        duration = 0;
    }

    public Order(Order o) {
        this.tableNumber = o.tableNumber;
        this.total = o.total;
        this.tax = o.tax;
        this.receipt = new ReceiptOrder(o.receipt);
        items.clear();
        items.addAll(o.items);
        duration = 0;
        this.cashierDone = o.cashierDone;
        this.cookDone = o.cookDone;
        this.amDone = o.amDone;
        this.type = o.type;
    }

    public Order(String tableNumber, List<Item> items) {
        this(tableNumber, items, "", "", ReceiptOrder.EMPTY);
    }

    public Order(Order o, List<Item> itemsWithIngredients, int duration) {
        this(o);
        this.items = itemsWithIngredients;
        this.duration = duration;

    }

    public Order(Order o, boolean paid, String cash) {
        this(o);
        this.cashierDone = paid;
        this.receipt = new ReceiptOrder(cash);
    }

    public String getTableNumber() {
        return tableNumber;
    }

    public List<Item> getItems() {
        return items;
    }

    public String getTotal() {
        return total;
    }

    public String getTax() {
        return tax;
    }

    public ReceiptOrder getReceipt() {
        return receipt;
    }

    public int getDuration() {
        return duration;
    }

    public String getId() {
        return getTableNumber();
    }

    public Order paid(String cash) {
        return new Order(this, true, cash);
    }

    public String getType() {
        return type;
    }
}
