import schema.Order;


public class MidgetHouse implements HandleOrders {
    private OrderDispatcher dispatcher;

    public MidgetHouse(OrderDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public void handle(Order order) {
        if("dodgy".equals(order.getType())){
            dispatcher.subscribe(order.getId(), new DodgyMidget(dispatcher));
        }
        if("regular".equals(order.getType())){
            dispatcher.subscribe(order.getId(), new RegularMidget(dispatcher));
        }
        dispatcher.dispatch(order.getId(),order);
    }
}
