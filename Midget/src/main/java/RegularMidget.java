import schema.Order;

public class RegularMidget implements HandleOrders {
    private OrderDispatcher dispatcher;

    public RegularMidget(OrderDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public void handle(Order order) {
        if (!order.isCookDone()) {
            dispatcher.dispatch(Cook.NAME, order);
        } else if (!order.isAmDone()) {
            dispatcher.dispatch(AssistantManager.NAME, order);
        } else if (!order.isCashierDone()) {
            dispatcher.dispatch(Cashier.NAME, order);
        } else {
            dispatcher.unSubscribe(order.getId());
        }
    }
}
