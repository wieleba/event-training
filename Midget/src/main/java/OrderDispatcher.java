import schema.Order;

import java.util.*;

public class OrderDispatcher {
    private Map<String, List<HandleOrders>> dictionary = new HashMap<String, List<HandleOrders>>();
    private Map<String, List<Order>> changes = new HashMap<String, List<Order>>();

    public void dispatch(String name, Order order) {
        List<HandleOrders> handleOrders = dictionary.get(name);
        List<Order> orders = changes.get(order.getId());
        if (null == orders) {
            orders = new ArrayList<Order>();
            changes.put(order.getId(), orders);
        }
        orders.add(order);

        for (HandleOrders handleOrder : handleOrders) {
            handleOrder.handle(order);
        }
    }

    public void subscribe(String name, HandleOrders... handlers) {
        List<HandleOrders> handleOrders = dictionary.get(name);
        if (handleOrders == null) {
            handleOrders = new LinkedList<HandleOrders>();
            dictionary.put(name, handleOrders);
        }
        handleOrders.addAll(Arrays.asList(handlers));
    }

    public void unSubscribe(String name) {
        System.out.println("Unsubscribe " + name);
        dictionary.remove(name);
    }
}
