import schema.Item;
import schema.Order;

import java.util.List;


public class TelephoneOperator {
    private HandleOrders handler;

    public TelephoneOperator(HandleOrders handler) {
        this.handler = handler;
    }

    public void createOrder(String tableNumber, List<Item> items) {
        System.out.println("Creating phone order for table " + tableNumber);

        handler.handle(new Order(tableNumber, items,"regular"));

    }

}
