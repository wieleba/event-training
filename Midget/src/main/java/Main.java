import schema.Item;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Arrays.asList;

public class Main {


    public static void main(String[] args) throws Exception {
        ExecutorService service = Executors.newCachedThreadPool();

        OrderDispatcher orderDispatcher = new OrderDispatcher();
        final Cashier cashier = new Cashier(new QueuedHandler(Cashier.NAME, new SendToDispatcherHandler(orderDispatcher)));
        final HandleOrders assistantHandlers = new AssistantManager(new QueuedHandler(AssistantManager.NAME,
                new SendToDispatcherHandler(orderDispatcher)));

        orderDispatcher.subscribe(Cashier.NAME, cashier);
        orderDispatcher.subscribe(AssistantManager.NAME, assistantHandlers);


        Statistic o = new QueuedHandler("Cook 1", new Cook(new SendToDispatcherHandler(orderDispatcher), 10));
        Statistic o1 = new QueuedHandler("Cook 2", new Cook(new SendToDispatcherHandler(orderDispatcher), 20));
        Statistic o2 = new QueuedHandler("Cook 3", new Cook(new SendToDispatcherHandler(orderDispatcher), 55));
        orderDispatcher.subscribe(Cook.NAME, new LoadBalancedQueuedHandler(o, o1, o2));

        MidgetHouse midgetHouse = new MidgetHouse(orderDispatcher);
        final Waiter w = new Waiter(midgetHouse);

        service.submit(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    w.createOrder(Integer.toString(i), asList(new Item(Thread.currentThread().getName(), 1, "10.0")));
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                    }
                }
            }
        });


        final TelephoneOperator operator = new TelephoneOperator(midgetHouse);
        service.submit(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    operator.createOrder(Integer.toString(100000 + i), asList(new Item(Thread.currentThread().getName(), 1, "10.0")));
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                    }
                }
            }
        });


        Thread.sleep(2000);

        cashier.payFor("1");
        cashier.payFor("2");
        cashier.payFor("7");
        cashier.payFor("4");
        cashier.payFor("12");
        cashier.payFor("111");

    }
}
