import schema.Item;
import schema.Order;

import java.util.List;


public class Waiter {
    private HandleOrders handler;

    public Waiter(HandleOrders handler) {
        this.handler = handler;
    }

    public void createOrder(String tableNumber, List<Item> items) {
        System.out.println("Creating order for table " + tableNumber);

        handler.handle(new Order(tableNumber, items, (Integer.valueOf(tableNumber) % 2 == 0) ? "regular" : "dodgy"));

    }

}
