import schema.Order;

public class SendToDispatcherHandler implements HandleOrders {
    private final OrderDispatcher orderDispatcher;

    public SendToDispatcherHandler(OrderDispatcher orderDispatcher) {
        this.orderDispatcher = orderDispatcher;
    }

    @Override
    public void handle(Order order) {
        orderDispatcher.dispatch(order.getId(), order);
    }

}
