import schema.Order;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class QueuedHandler implements HandleOrders, Statistic {
    private final HandleOrders handler;
    private Queue<Order> queue = new LinkedList<Order>();
    private final String name;

    private ExecutorService service = Executors.newCachedThreadPool();

    public QueuedHandler(String name, HandleOrders handler) {
        this.handler = handler;
        this.name = name;
        start();
    }

    private void start() {
        System.out.println("Queue Handler initialized");

        service.submit(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Order order = queue.poll();
                        if (null != order) {
                            handler.handle(order);
                        }
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        service.submit(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println(name + " contains " + queue.size());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {

                    }
                }
            }
        });
    }

    @Override
    public void handle(Order order) {
        this.queue.add(order);
    }

    @Override
    public int queueSize() {
        return queue.size();
    }
}
