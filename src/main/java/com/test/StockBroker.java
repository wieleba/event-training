package com.test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 06.02.13
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class StockBroker implements ITMessage {
    private ITMessage itMessage;
    private double currentPrice = 12.0;

    public StockBroker(ITMessage itMessage) {
        this.itMessage = itMessage;
    }

    /***/
    private List<PriceChangeMessage> priceChangesUp = new ArrayList<PriceChangeMessage>();

    private List<PriceChangeMessage> priceChangesDown = new ArrayList<PriceChangeMessage>();

    public void registerPriceChange(PriceChangeMessage priceChangeMessage) {
        priceChangesUp.add(priceChangeMessage);
        priceChangesDown.add(priceChangeMessage);

        itMessage.publish(new RemoveMessage(priceChangeMessage),15);
        decideWhatToDo();
    }

    private void decideWhatToDo() {
        if (allGreaterThanCurrent()) {
            updateCurrentPrice();
        }
        if (maxLowerThanCurrent()) {
            sell();
        }

    }

    private void sell() {
        //To change body of created methods use File | Settings | File Templates.
    }

    private boolean maxLowerThanCurrent() {
        return false;
    }

    private void updateCurrentPrice() {
        currentPrice = priceChangesUp.get(priceChangesUp.size() - 1).getPrice();
    }

    private boolean allGreaterThanCurrent() {
//        for (PriceChangeMessage priceChangeMessage : priceChangesUp){
//
//        }
        return true;
    }


    @Override
    public void publish(PriceChangeMessage message) {
        registerPriceChange(message);

    }

    public double getCurrentPrice() {
        return currentPrice;
    }
}
