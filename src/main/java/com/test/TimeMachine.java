package com.test;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 06.02.13
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
public interface TimeMachine {

    public Date getTime();
}
