package com.test;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 06.02.13
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public interface Consumer {

    public void consume(Message message);

}
