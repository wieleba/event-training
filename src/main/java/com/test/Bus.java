package com.test;

import com.test.Producer;

import java.lang.Object;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 06.02.13
 * Time: 10:40
 * To change this template use File | Settings | File Templates.
 */
public class Bus {
    private Consumer consumer;
    private Queue<Message> queue = new ConcurrentLinkedQueue<Message>();
    private final TimeMachine timeMachine;

    public Bus(TimeMachine timeMachine) {
        this.timeMachine = timeMachine;
    }


    public void schedule(Message message) {
        queue.add(message);
    }

    public void registerConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public void run() {
        while (!queue.isEmpty()) {
            Message peek = queue.peek();
            if (!timeMachine.getTime().before(peek.getArrivalTime())) {
                consumer.consume(queue.poll());
            }
        }

    }
}
