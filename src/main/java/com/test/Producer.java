package com.test;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 06.02.13
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */
public class Producer implements Consumer {
    private Message message;

    public Message getMessage() {
        return new Message("test");
    }

    public boolean messageReceived(Message message) {
        return message.equals(this.message);

    }

    @Override
    public void consume(Message message) {
        this.message = message;
    }
}
