package com.test;

/**
 * Created with IntelliJ IDEA.
 * User: cgm
 * Date: 06.02.13
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class PriceChangeMessage {
    private final double price;

    public PriceChangeMessage(double v) {
        this.price=v;
    }

    public double getPrice() {
        return price;
    }
}
