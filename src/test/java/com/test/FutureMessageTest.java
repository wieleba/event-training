package com.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class FutureMessageTest {

    @Test
    public void shouldSendMessage() throws Exception {
        //given
        Producer producer = new Producer();
        final Date arrivalTime = new Date(
                new Date().getTime() + TimeUnit.SECONDS.toMillis(10L)
        );
        Bus bus = new Bus(new TimeMachine() {
            @Override
            public Date getTime() {
                return arrivalTime;
            }
        });

        Message message = producer.getMessage();
        message.setArrivalTime(arrivalTime);

        bus.registerConsumer(producer);

        //when
        bus.schedule(message);
        bus.run();

        //then
        assertTrue(producer.messageReceived(message));
    }
}
